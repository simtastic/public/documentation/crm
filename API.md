# REST API

Most of api functions return JSON. POST, PATCH calls usually need some JSON data in payload.

Base URL of CRM API is: `api/v1/`. You need to prepend it to expressions in this reference. Example:

`GET http://crm_domain/api/v1/Contact/55643ca033f7ab4c5`.

## Authentication

### Authentication by Api Key

Authentication header:

`X-Api-Key: YOUR_API_KEY`

## CRUD Operations

#### List Entities

`GET {entityType}`

GET parameters:

* `maxSize` - (int) max size
* `offset` - (int) offset
* `where` - (array) filters
* `orderBy` - (string) field to sort by
* `order` - (string: 'asc' | 'desc') sort direction
* `select` - (string) list of fields to be returned sepratated by comma; if not specified, then all fields will be returned

_Example_

`GET Account?offset=0&maxSize=20`

Returns:
```
{
  "list": [... array of records...],
  "total": {totalCountOfRecords}
}
```

#### Read Entity

`GET {entityType}/{id}`

Returns attributes in JSON object.

_Example_

`GET Account/5564764442a6d024c`

#### Create Entity

`POST {entityType}`

Payload: Object of entity attributes.

Returns entity attributes in JSON object.

_Example_

`POST Account`

Payload:
```
{
  "name": "Test",
  "assignedUserId": "1"
}
```

#### Update Entity

`PUT {entityType}/{id}`

Payload: Object of entity attributes needed to be changed.

Returns attributes in JSON object.

_Example_

`PUT Account/5564764442a6d024c`

Payload:
```
{
  "assignedUserId": "1"
}
```

#### Delete Entity

`DELETE {entityType}/{id}`

_Example_

`DELETE Account/5564764442a6d024c`


## Related Entities

#### List Related Entities

`GET {entityType}/{id}/{link}`

* `offset` - (int) offset;
* `maxSize` - (int) max size;
* `where` - (array) filters;
* `sortBy` - (string) field to sort by;
* `asc` - (bool) sort direction.

_Example_

`GET Account/5564764442a6d024c/opportunities`

Returns:
```
{
  "list": [... array of records...],
  "total": {totalCountOfRecords}
}
```

#### Link Entity

`POST {entityType}/{id}/{link}`

Payload:

1. `id` attribute.
2. `ids` array attribute.
3. `"massRelate": true` and `"where": {...}` to relate multiple records by search criterias.

_Example_

`POST Account/5564764442a6d024c/opportunities`

Payload:
```
{
  "id": "55646fd85955c28c5"
}
```

#### Unlink Entity

`DELETE {entityType}/{id}/{link}`

Payload:

1. JSON with `id` attribute.
2. JSON with `ids` array attribute.

_Example_

`DELETE Account/5564764442a6d024c/opportunities`

Payload:
```
{
  "id": "55646fd85955c28c5"
}
```

